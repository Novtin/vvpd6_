import pytest

from vvpd5.vvpd4 import bold, itallic


@pytest.fixture(autouse=True)
def start_and_end_tests():
    print('Тест начат')


@pytest.mark.parametrize("string_1, output_1", [('*string*', '<em>string</em>'),
                                                ('*sad*', '<em>sad</em>')])
def test_good_bold(string_1, output_1):
    assert bold(string_1) == output_1


@pytest.mark.parametrize("string_1, output_1", [('**string**', '<strong>string</strong>'),
                                                ('**sad**', '<strong>sad</strong>'),
                                                ('****', '<strong></strong>')])
def test_good_itallic(string_1, output_1):
    assert itallic(string_1) == output_1


@pytest.mark.parametrize("string_1, output_1", [('*string*', '*string*'),
                                                ('*sad*', '*sad*'),
                                                ('hi', 'hi')])
def test_just_return_bold(string_1, output_1):
    assert itallic(string_1) == output_1


@pytest.mark.parametrize("string_1, output_1", [('*string*', '*string*'),
                                                ('*sad*', '*sad*'),
                                                ('hi', 'hi')])
def test_just_return_itallic(string_1, output_1):
    assert itallic(string_1) == output_1


@pytest.mark.parametrize("input_1, error", [(1, TypeError),
                                            (1.2, TypeError),
                                            ((1, 2, 3), TypeError),
                                            ([1, 2, 3], TypeError),
                                            ({3: 1, 5: 2}, TypeError)])
def test_error_bold(input_1, error):
    with pytest.raises(error):
        bold(input_1)


@pytest.mark.parametrize("input_1, error", [(1, TypeError),
                                            (1.2, TypeError),
                                            ((1, 2, 3), TypeError),
                                            ([1, 2, 3], TypeError),
                                            ({7: 1, 1: 2}, TypeError)])
def test_error_itallic(input_1, error):
    with pytest.raises(error):
        itallic(input_1)
