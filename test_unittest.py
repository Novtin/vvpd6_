import unittest

from vvpd5.vvpd4 import bold, itallic


class MainTest(unittest.TestCase):
    def setUp(self):
        print('Тест начат')

    def tearDown(self):
        print('Тест закончен')

    def test_good_bold(self):
        self.assertEqual(bold('*string*'), '<em>string</em>')

    def test_good_itallic(self):
        self.assertEqual(itallic('**string**'), '<strong>string</strong>')

    def test_just_return_bold(self):
        self.assertEqual(bold('string'), 'string')

    def test_just_return_itallic(self):
        self.assertEqual(itallic('string'), 'string')

    def test_error_bold(self):
        with self.assertRaises(TypeError):
            bold(1)

    def test_error_itallic(self):
        with self.assertRaises(TypeError):
            itallic(1)
