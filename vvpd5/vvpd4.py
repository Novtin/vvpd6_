"""
Устинов Данил КИ21-17/2Б вариант 9
"""


def bold(string: str) -> str:
    """
    Функция, которая переводит введенную строку, на концах которой
    по одной *, в строку, на концах которой <em> и </em>.
    Если же не выполняется это условие, то возращается изначальная строка
    :param string: изначальная строка
    :type string: str
    :return: строка, на концах которой <em> и </em>, или изначальная строка
    :rtype: str
    """
    string = list(string)
    if len(string) > 1:
        if string[0] == string[-1] == "*":
            string[0] = '<em>'
            string[-1] = '</em>'
    return ''.join(string)


def itallic(string: str) -> str:
    """
    Функция, которая переводит введенную строку, на концах которой
    по две *, в строку, на концах которой <strong> и </strong>.
    Если же не выполняется это условие, то возращается изначальная строка
    :param string: изначальная строка
    :type string: str
    :return: строка, на концах которой <strong> и </strong>, или изначальная строка
    :rtype: str
    """
    string = list(string)
    if len(string) > 3:
        if string[0] == string[1] == string[-2] == string[-1] == "*":
            del string[0], string[0], string[-1], string[-1]
            string.insert(0, '<strong>')
            string.append('</strong>')
    return ''.join(string)


# string_bold = input('Строка для первой функции: ')
# string_itallic = input('Строка для второй функции: ')
# print(f'Первая строка: {bold(string_bold)}')
# print(f'Вторая строка: {itallic(string_itallic)}')
